<?php
/**
 * @file
 * getstudio_core.slick_default_preset.inc
 */

/**
 * Implements hook_slick_default_presets().
 */
function getstudio_core_slick_default_presets() {
  $export = array();

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'clone_of_default';
  $preset->label = 'Front Slider';
  $preset->breakpoints = 0;
  $preset->skin = '';
  $preset->options = array(
    'general' => array(
      'normal' => '',
      'thumbnail' => '',
      'template_class' => '',
      'goodies' => array(
        'arrow-down' => 0,
        'pattern' => 0,
        'random' => 0,
      ),
      'arrow_down_target' => '',
      'arrow_down_offset' => '',
    ),
    'settings' => array(
      'mobileFirst' => FALSE,
      'asNavFor' => '',
      'accessibility' => TRUE,
      'adaptiveHeight' => FALSE,
      'autoplay' => FALSE,
      'autoplaySpeed' => 3000,
      'pauseOnHover' => TRUE,
      'pauseOnDotsHover' => FALSE,
      'arrows' => TRUE,
      'prevArrow' => '<button type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>',
      'nextArrow' => '<button type="button" data-role="none" class="slick-next" aria-label="next">Next</button>',
      'centerMode' => TRUE,
      'centerPadding' => '',
      'dots' => FALSE,
      'dotsClass' => 'slick-dots',
      'appendDots' => '$(element)',
      'draggable' => TRUE,
      'fade' => FALSE,
      'focusOnSelect' => FALSE,
      'infinite' => TRUE,
      'initialSlide' => 0,
      'lazyLoad' => 'ondemand',
      'respondTo' => 'window',
      'rtl' => FALSE,
      'rows' => 1,
      'slidesPerRow' => 1,
      'slide' => '',
      'slidesToShow' => 1,
      'slidesToScroll' => 1,
      'speed' => 500,
      'swipe' => TRUE,
      'swipeToSlide' => FALSE,
      'edgeFriction' => 0.34999999999999998,
      'touchMove' => TRUE,
      'touchThreshold' => 5,
      'useCSS' => TRUE,
      'cssEase' => 'ease',
      'cssEaseBezier' => '',
      'cssEaseOverride' => '',
      'useTransform' => FALSE,
      'easing' => 'linear',
      'variableWidth' => FALSE,
      'vertical' => FALSE,
      'verticalSwiping' => FALSE,
      'waitForAnimate' => TRUE,
    ),
  );
  $export['clone_of_default'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'our_team_slide';
  $preset->label = 'Our Team Slide';
  $preset->breakpoints = 3;
  $preset->skin = 'fullwidth';
  $preset->options = array(
    'general' => array(
      'normal' => '',
      'thumbnail' => '',
      'template_class' => '',
      'goodies' => array(
        'arrow-down' => 0,
        'pattern' => 0,
        'random' => 0,
      ),
      'arrow_down_target' => '',
      'arrow_down_offset' => '',
    ),
    'settings' => array(
      'mobileFirst' => FALSE,
      'asNavFor' => '',
      'accessibility' => TRUE,
      'adaptiveHeight' => FALSE,
      'autoplay' => FALSE,
      'autoplaySpeed' => 3000,
      'pauseOnHover' => TRUE,
      'pauseOnDotsHover' => FALSE,
      'arrows' => TRUE,
      'prevArrow' => '<button type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>',
      'nextArrow' => '<button type="button" data-role="none" class="slick-next" aria-label="next">Next</button>',
      'centerMode' => FALSE,
      'centerPadding' => '50px',
      'dots' => FALSE,
      'dotsClass' => 'slick-dots',
      'appendDots' => '$(element)',
      'draggable' => TRUE,
      'fade' => FALSE,
      'focusOnSelect' => FALSE,
      'infinite' => TRUE,
      'initialSlide' => 0,
      'lazyLoad' => 'ondemand',
      'respondTo' => 'window',
      'rtl' => FALSE,
      'rows' => 1,
      'slidesPerRow' => 1,
      'slide' => '',
      'slidesToShow' => 3,
      'slidesToScroll' => 1,
      'speed' => 500,
      'swipe' => TRUE,
      'swipeToSlide' => FALSE,
      'edgeFriction' => 0.34999999999999998,
      'touchMove' => TRUE,
      'touchThreshold' => 5,
      'useCSS' => TRUE,
      'cssEase' => 'ease',
      'cssEaseBezier' => '',
      'cssEaseOverride' => '',
      'useTransform' => FALSE,
      'easing' => 'linear',
      'variableWidth' => FALSE,
      'vertical' => FALSE,
      'verticalSwiping' => FALSE,
      'waitForAnimate' => TRUE,
    ),
    'responsives' => array(
      'responsive' => array(
        0 => array(
          'breakpoint' => 767,
          'unslick' => FALSE,
          'settings' => array(
            'accessibility' => TRUE,
            'adaptiveHeight' => FALSE,
            'autoplay' => FALSE,
            'autoplaySpeed' => 3000,
            'pauseOnHover' => FALSE,
            'pauseOnDotsHover' => FALSE,
            'arrows' => FALSE,
            'centerMode' => FALSE,
            'centerPadding' => '50px',
            'dots' => FALSE,
            'draggable' => TRUE,
            'fade' => FALSE,
            'focusOnSelect' => FALSE,
            'infinite' => TRUE,
            'initialSlide' => 0,
            'rows' => 1,
            'slidesPerRow' => 1,
            'slidesToShow' => 2,
            'slidesToScroll' => 1,
            'speed' => 500,
            'swipe' => TRUE,
            'swipeToSlide' => FALSE,
            'edgeFriction' => 0.34999999999999998,
            'touchMove' => TRUE,
            'touchThreshold' => 5,
            'useCSS' => TRUE,
            'cssEase' => 'ease',
            'cssEaseBezier' => '',
            'cssEaseOverride' => '',
            'useTransform' => FALSE,
            'easing' => 'linear',
            'variableWidth' => FALSE,
            'vertical' => FALSE,
            'verticalSwiping' => FALSE,
            'waitForAnimate' => TRUE,
          ),
        ),
        1 => array(
          'breakpoint' => 0,
          'unslick' => FALSE,
          'settings' => array(
            'accessibility' => FALSE,
            'adaptiveHeight' => FALSE,
            'autoplay' => FALSE,
            'autoplaySpeed' => 3000,
            'pauseOnHover' => FALSE,
            'pauseOnDotsHover' => FALSE,
            'arrows' => FALSE,
            'centerMode' => FALSE,
            'centerPadding' => '50px',
            'dots' => FALSE,
            'draggable' => FALSE,
            'fade' => FALSE,
            'focusOnSelect' => FALSE,
            'infinite' => FALSE,
            'initialSlide' => 0,
            'rows' => 1,
            'slidesPerRow' => 1,
            'slidesToShow' => 1,
            'slidesToScroll' => 1,
            'speed' => 500,
            'swipe' => FALSE,
            'swipeToSlide' => FALSE,
            'edgeFriction' => 0.34999999999999998,
            'touchMove' => FALSE,
            'touchThreshold' => 5,
            'useCSS' => FALSE,
            'cssEase' => 'ease',
            'cssEaseBezier' => '',
            'cssEaseOverride' => '',
            'useTransform' => FALSE,
            'easing' => 'linear',
            'variableWidth' => FALSE,
            'vertical' => FALSE,
            'verticalSwiping' => FALSE,
            'waitForAnimate' => FALSE,
          ),
        ),
        2 => array(
          'breakpoint' => 0,
          'unslick' => FALSE,
          'settings' => array(
            'accessibility' => FALSE,
            'adaptiveHeight' => FALSE,
            'autoplay' => FALSE,
            'autoplaySpeed' => 3000,
            'pauseOnHover' => FALSE,
            'pauseOnDotsHover' => FALSE,
            'arrows' => FALSE,
            'centerMode' => FALSE,
            'centerPadding' => '50px',
            'dots' => FALSE,
            'draggable' => FALSE,
            'fade' => FALSE,
            'focusOnSelect' => FALSE,
            'infinite' => FALSE,
            'initialSlide' => 0,
            'rows' => 1,
            'slidesPerRow' => 1,
            'slidesToShow' => 1,
            'slidesToScroll' => 1,
            'speed' => 500,
            'swipe' => FALSE,
            'swipeToSlide' => FALSE,
            'edgeFriction' => 0.34999999999999998,
            'touchMove' => FALSE,
            'touchThreshold' => 5,
            'useCSS' => FALSE,
            'cssEase' => 'ease',
            'cssEaseBezier' => '',
            'cssEaseOverride' => '',
            'useTransform' => FALSE,
            'easing' => 'linear',
            'variableWidth' => FALSE,
            'vertical' => FALSE,
            'verticalSwiping' => FALSE,
            'waitForAnimate' => FALSE,
          ),
        ),
      ),
    ),
  );
  $export['our_team_slide'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'testimonials_slide';
  $preset->label = 'Testimonials';
  $preset->breakpoints = 0;
  $preset->skin = '';
  $preset->options = array(
    'general' => array(
      'normal' => '',
      'thumbnail' => '',
      'template_class' => '',
      'goodies' => array(
        'arrow-down' => 0,
        'pattern' => 0,
        'random' => 0,
      ),
      'arrow_down_target' => '',
      'arrow_down_offset' => '',
    ),
    'settings' => array(
      'mobileFirst' => FALSE,
      'asNavFor' => '',
      'accessibility' => TRUE,
      'adaptiveHeight' => FALSE,
      'autoplay' => TRUE,
      'autoplaySpeed' => 3000,
      'pauseOnHover' => TRUE,
      'pauseOnDotsHover' => FALSE,
      'arrows' => FALSE,
      'prevArrow' => '<button type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>',
      'nextArrow' => '<button type="button" data-role="none" class="slick-next" aria-label="next">Next</button>',
      'centerMode' => FALSE,
      'centerPadding' => '50px',
      'dots' => FALSE,
      'dotsClass' => 'slick-dots',
      'appendDots' => '$(element)',
      'draggable' => TRUE,
      'fade' => FALSE,
      'focusOnSelect' => FALSE,
      'infinite' => TRUE,
      'initialSlide' => 0,
      'lazyLoad' => 'ondemand',
      'respondTo' => 'window',
      'rtl' => FALSE,
      'rows' => 1,
      'slidesPerRow' => 1,
      'slide' => '',
      'slidesToShow' => 1,
      'slidesToScroll' => 1,
      'speed' => 500,
      'swipe' => TRUE,
      'swipeToSlide' => FALSE,
      'edgeFriction' => 0.34999999999999998,
      'touchMove' => TRUE,
      'touchThreshold' => 5,
      'useCSS' => TRUE,
      'cssEase' => 'ease',
      'cssEaseBezier' => '',
      'cssEaseOverride' => '',
      'useTransform' => FALSE,
      'easing' => 'linear',
      'variableWidth' => FALSE,
      'vertical' => FALSE,
      'verticalSwiping' => FALSE,
      'waitForAnimate' => TRUE,
    ),
  );
  $export['testimonials_slide'] = $preset;

  return $export;
}
